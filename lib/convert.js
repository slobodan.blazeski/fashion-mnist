const fs = require('fs');

const dataDir = './data/';
const trainImageFile = dataDir + 'train-images-idx3-ubyte';
const testImageFile = dataDir + 't10k-images-idx3-ubyte';
const trainLabelFile = dataDir + 'train-labels-idx1-ubyte';
const testLabelFile = dataDir + 't10k-labels-idx1-ubyte';
const trainEntries = 60000;
const testEntries = 10000;

const create2DArray = (numRows, numColumns) => {
	let array = new Array(numRows); 
	for(let i = 0; i < numColumns; i++) {
		array[i] = new Array(numColumns); 
	}
	return array; 
};


const convert = (images, labels, entries) => {  
    const dataFileBuffer = fs.readFileSync(images);
    const labelFileBuffer = fs.readFileSync(labels);
    const pixelValues     = [];

    for (let image = 0; image < entries; image++) {
        const pixels = create2DArray(28, 28);
        for (let x = 0; x <= 27; x++) {
            for (let y = 0; y <= 27; y++) {
                pixels[y][x]= dataFileBuffer[(image * 28 * 28) + (x + (y * 28)) + 15] 
            }
        }
        const label = JSON.stringify(labelFileBuffer[image + 8]);
        const data = [].concat.apply([], pixels);
        const imageData = {
            label,
            data
        };
        pixelValues.push(imageData);
    }
    return pixelValues;
}

exports.fashionData = {
    train: convert(trainImageFile, trainLabelFile, trainEntries),
    test: convert(testImageFile, testLabelFile, testEntries)
};
