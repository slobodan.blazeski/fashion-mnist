const {fashionData} = require('./convert');

class FashionData {
  constructor() {
    this.trainSize = fashionData.train.length;
    this.testSize = fashionData.test.length;
    this.trainBatchIndex = 0;
    this.testBatchIndex = 0;
  }

  resetTraining() {
    this.trainBatchIndex = 0;
    return true;
  }

  resetTest() {
    this.testBatchIndex = 0;
    return true;
  }

  hasMoreTrainingData() {
    return this.trainBatchIndex < this.trainSize;
  }

  hasMoreTestData() {
    return this.testBatchIndex < this.testSize;
  }

  nextTrainBatch(batchSize) {
    if (this.trainBatchIndex + batchSize < this.trainSize) {
      const batch = fashionData.train.slice(this.trainBatchIndex, 
                                            this.trainBatchIndex + batchSize);
      this.trainBatchIndex += batchSize;
      return batch;
    }
    return [];
  }

  nextTestBatch(batchSize) {
    if (this.testBatchIndex + batchSize < this.testSize) {
      const batch = fashionData.test.slice(this.testBatchIndex, 
                                           this.testBatchIndex + batchSize);
      this.testBatchIndex += batchSize;
      return batch;  
    }
    return [];
  }
}
module.exports = new FashionData();
