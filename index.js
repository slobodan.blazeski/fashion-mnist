const express =  require('express');
const app = express();
const fashion = require('./lib/fashion');

app.use(express.static('public'));

app.get('/api/nextTrainBatch/:batchSize', (req, res) => {
    res.json(fashion.nextTrainBatch(parseInt(req.params.batchSize)));
});

app.get('/api/nextTestBatch/:batchSize', (req, res) => {
    res.json(fashion.nextTestBatch(parseInt(req.params.batchSize)));
});

app.get('/api/hasMoreTrainingData', (req, res) => {
    res.json(fashion.hasMoreTrainingData());
});

app.get('/api/hasMoreTestData', (req, res) => {
    res.json(fashion.hasMoreTestData());
});

app.get('/api/resetTraining', (req, res) => {
    res.json(fashion.resetTraining());
});

app.get('/api/resetTest', (req, res) => {
    res.json(fashion.resetTest());
});



app.listen(3000);
console.log('App listens on http://localhost:3000')
