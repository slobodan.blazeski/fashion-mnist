/*
function visualize(fashionTrain) {
    var samplesCount = 10;
    var size = 28;
    var canvas = document.getElementById("mnistCanvas");
    canvas.height = size*samplesCount;
    var ctx = canvas.getContext("2d");

    var canvasData = ctx.getImageData(0, 0, canvas.width, canvas.height);
    var colOffset, rowOffcet;

    for (var col=0; col < 10; col++) {
        colOffset = col*size;
        var digits = fashionTrain.slice(col * samplesCount, (col + 1) * samplesCount);
        if (digits.length !== 0) {                
            for (var row=0;row < samplesCount; row++) {
                var digit = digits[row].data;
                rowOffcet = row*size;
                for (var i = 0; i < digit.length; i++) {
                    var pointColor = digit[i];
                    var position = (colOffset+(rowOffcet + Math.floor(i/size))*canvas.width) + (i % size);
                    canvasData.data[position * 4 + 0] = 255-pointColor;
                    canvasData.data[position * 4 + 1] = 255-pointColor;
                    canvasData.data[position * 4 + 2] = 255-pointColor;
                    canvasData.data[position * 4 + 3] = 255;
                }
            }
        }
    }
    ctx.putImageData(canvasData, 0, 0);
}
visualize(fashionTest.slice(200));
*/
const fahionMnistLabels = ["T-shirt/top",  // index 0
                           "Trouser",      // index 1
                           "Pullover",     // index 2 
                           "Dress",        // index 3 
                           "Coat",         // index 4
                           "Sandal",       // index 5
                           "Shirt",        // index 6 
                           "Sneaker",      // index 7 
                           "Bag",          // index 8 
                           "Ankle boot"];  // index 9

const labelNo = 10;                           

const oneHot = d => {
    let arr = new Array(labelNo);
    arr.fill(0);
    arr[d] = 1;
    return arr;
}                         
                           


// Model
const model = tf.sequential();

model.add(tf.layers.conv2d({
    filters: 64, 
    kernelSize: 2, 
    padding: 'same', 
    activation: 'relu', 
    inputShape: [28,28,1]
})); 
model.add(tf.layers.maxPooling2d({poolSize:2}));
model.add(tf.layers.dropout(0.3));

model.add(tf.layers.conv2d({
    filters: 32, 
    kernelSize: 2, 
    padding: 'same', 
    activation: 'relu'
}));
model.add(tf.layers.maxPooling2d({poolSize: 2}));
model.add(tf.layers.dropout(0.3));

model.add(tf.layers.flatten())
model.add(tf.layers.dense({
    units: 256, 
    activation: 'relu'
}));
model.add(tf.layers.dropout(0.5));
model.add(tf.layers.dense({
    units: 10, 
    activation: 'softmax'
}));


const optimizer = tf.train.adam();
model.compile({
  optimizer: optimizer,
  loss: 'categoricalCrossentropy',
  metrics: ['accuracy'],
});

const NUM_EPOCHS = 10;
const BATCH_SIZE = 100;
const TEST_SIZE = 50;

const HOST = 'http://localhost:3000/api/';

const trainRatio = 0.9;
const height = 28;
const width = 28;


const prepare = d => {
    let res = math.reshape(d.data, [width, height]);
    for (let x = 0; x < width; ++x) {
        for (let y = 0; y < height; ++y) {
            res[x][y] = [res[x][y] / 255];        
        }        
    }
    return res;
};

async function train() {
    await fetch(HOST+'resetTraining');
    let step = 0;
    let hasMoreTrainingDataResponse = await fetch(HOST+'hasMoreTrainingData');
    let hasMoreTrainingData = await hasMoreTrainingDataResponse.json();
    let batchResponse = await fetch(HOST+'nextTrainBatch/' + BATCH_SIZE);
    let batch = await batchResponse.json();
    while (hasMoreTrainingData && batch.length !== 0) {
        const x_train = batch.map(prepare);
        const y_train = batch.map(d => oneHot(d.label));
        
        const image = tf.tensor(x_train, [BATCH_SIZE, width, height, 1]);
        const label = tf.tensor(y_train, [BATCH_SIZE, fahionMnistLabels.length]);
        const history = await model.fit(
            image, label, {batchSize: BATCH_SIZE, shuffle: true});
        if (step % 20 === 0) {
            const loss = history.history.loss[0].toFixed(6);
            const acc = history.history.acc[0].toFixed(4);
            console.log(`  - step: ${step}: loss: ${loss}, accuracy: ${acc}`);
        }
        step++;
        hasMoreTrainingDataResponse = await fetch(HOST+'hasMoreTrainingData');
        hasMoreTrainingData = await hasMoreTrainingDataResponse.json();
        batchResponse = await fetch(HOST+'nextTrainBatch/' + BATCH_SIZE);
        batch = await batchResponse.json();        
    }
    return step;
}

async function test() {
    await fetch(HOST+'resetTest');

    let hasMoreTestDataResponse = await fetch(HOST+'hasMoreTestData');
    let hasMoreTestData = await hasMoreTestDataResponse.json();
    let batchResponse = await fetch(HOST+'nextTestBatch/' + BATCH_SIZE);
    let batch = await batchResponse.json();
    while (hasMoreTestData && batch.length !== 0) {
        const x_test = batch.map(prepare);
        const y_test = batch.map(d => oneHot(d.label));

        const image = tf.tensor(x_test, [BATCH_SIZE, width, height, 1]);
        const output = model.predict(image);
        const predictions = output.argMax(1).dataSync();
        const labels = tf.tensor2d(y_test, [
            BATCH_SIZE, fahionMnistLabels.length]).argMax(1).dataSync();
    
        let correct = 0;
        for (let i = 0; i < TEST_SIZE; i++) {
            if (predictions[i] === labels[i]) {
                correct++;
            }
        }
        const accuracy = ((correct / TEST_SIZE) * 100).toFixed(2);
        console.log(`* Test set accuracy: ${accuracy}%\n`);

        hasMoreTestDataResponse = await fetch(HOST+'hasMoreTestData');
        hasMoreTestData = await hasMoreTestDataResponse.json();
        batchResponse = await fetch(HOST+'nextTestBatch/' + BATCH_SIZE);
        batch = await batchResponse.json();        
    }
}

async function run() {
    for (let i = 0; i < NUM_EPOCHS; i++) {
      const trainSteps = await train();
      test();
    }
    console.log(`**** Trained ${NUM_EPOCHS} epochs`);
  }

run();
